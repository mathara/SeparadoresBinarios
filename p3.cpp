/*
Matheus Mendes Araujo 156737

*/
#include <iostream>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
using namespace std;


typedef long int umorelong;
umorelong count = 0;
//imprime vetor
void print(vector<umorelong> v){
  for (umorelong i = 0; i < v.size(); i++) {
    printf("%ld ", v[i] );
  }
  printf("\n");
}

//realiza troca de dos valores
void troca(umorelong *a, umorelong *b)
{
    umorelong temp = *a;
    *a = *b;
    *b = temp;
}

//particiona o vetor, sendo o ultimo elemento o pivo
umorelong particionaNormal(vector<umorelong> &v, umorelong p, umorelong r){
  umorelong pivo;
  umorelong i;

  pivo = v[r];
  //printf("pivo %d\n", pivo);
  i = p -1;

  for (umorelong j = p; j < r; j++) {
    /* code */
    if (v[j]<= pivo){
      i = i+1;
      troca(&v[i], &v[j]);
    }
  }

  troca(&v[i+1], &v[r]);

  return i+1;
}

//particiona Aleatório
umorelong particionaAleat (vector<umorelong> &v, umorelong p, umorelong r){
  umorelong j;
  //semente de aleatoriedade
  srand(0);

  //torna o pivo aleatorizado
  j = rand() % (r-p+1) + p;
  troca(&v[r], &v[j]);
  return  particionaNormal(v,p,r);
}

//seleciona Aleatório
umorelong selecionaAleat(vector<umorelong> &v, umorelong p, umorelong r, umorelong i){
  umorelong q, k;
  if(p == r){
    return v[p];
  }

  q = particionaAleat(v,p,r);
  k = q - p + 1;

  if (i == k){
    return v[q];
  }
  else if (i < k){
    return selecionaAleat(v,p,q-1,i);
  }
  else {
    return selecionaAleat(v,q+1,r,i-k);
  }
}

// quickSortAleat
void quickSortAleat(vector<umorelong> &v, umorelong p, umorelong r) {
  umorelong q;
  if (p < r) {
    q = particionaAleat(v,p,r);
    quickSortAleat(v,p,q-1);
    quickSortAleat(v,q+1,r);
  }
}

//separadores binarios
void separadores_binario(vector<umorelong> &v, umorelong p, umorelong r, umorelong k,vector<umorelong> &sp ){
  umorelong lugar, med;

  //procura a mediana das duas metades do vetor
  if (k != 0){
    //pega a primeira mediana
    med = (r + p)/2;
    lugar = (r - p)/2 +1;
    k = (k-1)/2;
    //sp.push_back(selecionaAleat(v, p, r, lugar));
    sp[count++] = selecionaAleat(v, p, r, lugar);
    separadores_binario(v, p, med-1, k,sp );
    //sp.push_back(selecionaAleat(v, p, r, lugar));
    //printf("%d\n", selecionaAleat(v, p, r, lugar));
    separadores_binario(v, med+1, r, k,sp );
  }
  // else{
  //   sp.push_back(selecionaAleat(v, p, r, lugar));
  //   //printf("%d\n", selecionaAleat(v, p, r, lugar));
  // }
}



int main() {
  //variáveis
  umorelong size_vet = 0;
  umorelong separadores_bin = 0;
  vector<umorelong> v;
  vector<umorelong> sp;

  //recebe tamanho vector
  cin >> size_vet;
  //recebe a quantidade de separadores_bin
  cin >> separadores_bin;
  //redimensiona o vector
  v.resize(size_vet);
  sp.resize(separadores_bin);

  //recebe valores do vector
  for (umorelong i = 0; i < v.size(); i++) {
    cin >> v[i];
  }
  //print(v);
  //particionaAleat(v, 0, (size_vet -1));
  //printf("Numero menor %d\n", selecionaAleat(v, 0, size_vet-1, 5) );
  separadores_binario(v, 0, (size_vet-1), separadores_bin ,sp );
  quickSortAleat(sp,0, (sp.size()-1));
  //print(v);
  print(sp);

  // for (int i = 0; i < separadores_bin; i++) {
  //   /* code */
  //   printf("%d\n",selecionaAleat(v, 0, size_vet-1, sp[i]));
  // }
}
